"use strict";

var _ = require('lodash');

function emergencyLog() {
  console.error.apply(console, arguments);
}

function getBucketFromUserId(userId) {
  var lastChar = userId.toString().slice(-1);
  return (parseInt(lastChar+'', 16) % 2) ? 'A' : 'B';
}

function configureLogger(statsHandlers, config, logger) {
  statsHandlers.event.push(function(eventName, properties) {
    if (!properties) return;

    // Strip out dodgy named attributes which may screw with Kibana
    var meta = _.omit(properties, 'agent:type', 'agent:family', 'agent:version',
      'agent:device:family', 'agent:os:family',
      'agent:os:version');

    meta.eventName = eventName;

    logger.info('stats.event', meta);
  });
}

function configureCube(statsHandlers, config) {
  var Cube = require("@gitterhq/cube");
  var statsUrl = config.get("stats:cube:cubeUrl");
  var cube = Cube.emitter(statsUrl);

  statsHandlers.event.push(function(eventName, properties) {
    if(!properties) properties = {};

    properties.env = config.runtimeEnvironment;

    var event = {
      type: "gitter_" + eventName.replace(/\./g, '_'),
      time: new Date(),
      data: properties
    };
    cube.send(event);
  });

  statsHandlers.charge.push(function (userId, usd) {

    var event = {
      type: 'gitter_charge',
      time: new Date(),
      data: {
        userId: userId,
        usd: usd,
        env: config.runtimeEnvironment
      }
    };
    cube.send(event);
  });

}

function configureStatsD(statsHandlers, config) {
  var statsdClient = require('./stats-client').create({
      config: config,
      prefix: config.get('stats:statsd:prefix')
    });

  statsHandlers.event.push(function(eventName) {
    statsdClient.increment(eventName);
  });

  statsHandlers.charge.push(function (userId, usd) {
    statsdClient.increment('charged', usd);
  });

  statsHandlers.eventHF.push(function(eventName, count, frequency, tags) {
    if(!count) count = 1;
    if(!frequency) frequency = 0.1;

    /* Only send to the server one tenth of the time */
    statsdClient.increment(eventName, count, frequency, tags);
  });

  statsHandlers.gaugeHF.push(function(gaugeName, value, frequency, tags) {
    if(!frequency) frequency = 0.1;

    /* Only send to the server one tenth of the time */
    statsdClient.gauge(gaugeName, value, frequency, tags);
  });

  statsHandlers.responseTime.push(function(timerName, duration, tags) {
    statsdClient.timing(timerName, duration, 1, tags);
  });
}

function configureIntercom(statsHandlers, config) {
  var intercomWhiteList = {
    new_chat: true,
    user_login:  true,
    join_room: true,
    create_room: true,
    create_group: true,
    new_group: true,
    join_group: true,
    leave_group: true,
    user_added_someone: true,
    new_invited_user: true,
    invite_accepted: true,
    new_user: true,
    charge_failed: true,
    lurk_room: true,
    gitter_supporter: true,
    gitlab_ml_opt_in: true
  };

  var Intercom = require('intercom.io');
  var intercomOptions = {
    apiKey: config.get("stats:intercom:key"),
    appId: config.get("stats:intercom:app_id")
  };

  var intercom = new Intercom(intercomOptions);

  statsHandlers.event.push(function(eventName, properties) {

    /**
     * Metadata can be used to submit an event with extra key value data.
     * Each event can contain up to five metadata key values.
     * https://doc.intercom.io/api#event-model
     */
    function selectIntercomMetaData(properties) {
      var result = {};
      var keys = Object.keys(properties);
      var totalKeys = 0;
      for (var i = 0; i < keys.length && totalKeys <= 5; i++) {
        var key = keys[i];
        if (key === 'userId') continue; // Already have this in the event

        var value = properties[key];
        var valueType = typeof value;
        if (valueType === 'string' || valueType === 'number' || valueType === 'boolean') {
          result[key] = value;
        }
      }
      return result;
    }

    if(!intercomWhiteList[eventName]) return;
    // intercom likes a seconds since epoch date not ms that node gives by default
    var now = Number(Date.now()/1000).toFixed(0);
    var attemptCount = 0;
    var eventData = {
      "event_name": eventName,
      "created_at": now,
      "user_id": properties.userId,
    };

    if (properties) {
      eventData.metadata = selectIntercomMetaData(properties);
    }

    function createEvent() {
      intercom.createEvent(eventData, function(err, res) {
        if (err || res.errors && res.errors.length) {
          attemptCount++;
          if (attemptCount < 2) {
            setTimeout(createEvent, 2000);
          } else {
              emergencyLog('Intercom error: ' + err, { exception: err });
          }
        }
      });
    }
    createEvent();
  });

  statsHandlers.charge.push(function (userId, usd) {
    // intercom likes a seconds since epoch date not ms that node gives by default
    var now = Number(Date.now()/1000).toFixed(0);
    var attemptCount = 0;
    function createEvent() {
      intercom.createEvent({
        "event_name": 'charged',
        "created_at": now,
        "user_id": userId,
        "metadata": {
          "value": usd
        }
      }, function (err, res) {
        if (err || res.errors && res.errors.length) {
          attemptCount++;
          if (attemptCount < 2) {
            setTimeout(createEvent, 2000);
          } else {
            emergencyLog('Intercom error: ' + err, { exception: err });
          }
        }
      });
    }
    createEvent();
  });

  statsHandlers.userUpdate.push(function(user/*, properties*/) {
    var created_at = new Date(user._id.getTimestamp());

    var profile = {
      "email": user.email,
      "user_id": user._id,
      "name": user.displayName,
      "created_at": created_at,
      //"last_impression_at": now,
      "update_last_request_at": true,
      "custom_data": {
        "username": user.username,
        "bucket": getBucketFromUserId(user._id),
        "state": user.state || 'ACTIVE',
      }
    };

    intercom.createUser(profile, function(err) {
      if (err) {
        emergencyLog('Intercom error: ' + err, { exception: err });
      }
    });
  });

}

function configureMixpanel(statsHandlers, config) {
  var mixpanelEventBlacklist = {
    location_submission: true,
    push_notification: true,
    mail_bounce: true,
    new_troupe: true,
    new_mail_attachment: true,
    remailed_email: true,
    new_file_version: true,
    new_file: true,
    login_failed: true,
    password_reset_invalid: true,
    password_reset_completed: true,
    invite_reused: true,
    confirmation_reused: true,
    client_error_404: true,
    client_error_4xx: true,
    client_error_5xx: true,
    unread_notification_sent: true,
    charge_failed: true,
    suggest_room: true,
    topics_notification_email: true,
    topic_notification_email: true,
    reply_notification_email: true,
    comment_notification_email: true,
  };

  var Mixpanel = require('mixpanel');
  var token = config.get("stats:mixpanel:token");
  var mixpanel = Mixpanel.init(token);

  statsHandlers.alias.push(function(distinctId, userId, cb) {
    mixpanel.alias(distinctId, userId, cb);
  });

  statsHandlers.event.push(function(eventName, properties) {

    if(!properties || !(properties.userId || properties.distinctId)) {
      return;
    }

    if(mixpanelEventBlacklist[eventName]) {
      return;
    }

    properties.distinct_id = properties.distinctId || properties.userId;

    mixpanel.track(eventName, properties, function(err) {
      if (err) emergencyLog('Mixpanel Track error: ' + err, { exception: err });
    });
  });

  statsHandlers.charge.push(function (userId, usd) {
    if (!userId) return;
    mixpanel.people.track_charge(userId, usd, {}, function (err) {
      if (err) return emergencyLog('Mixpanel Charge error: ' + err, { exception: err });
    });
  });

  statsHandlers.userUpdate.push(function(user, properties) {
    properties = properties || {};

    var createdAt = Math.round(user._id.getTimestamp().getTime());

    var mp_properties = {
      $created_at:  new Date(createdAt).toISOString(),
      $name:        user.displayName,
      $username:    user.username
    };

    if (user.email) mp_properties.$email = user.email;

    properties.bucket = getBucketFromUserId(user._id);

    for (var attr in properties) {
      var value = properties[attr] instanceof Date ? properties[attr].toISOString() : properties[attr];
      mp_properties[attr] = value;
    }

    mixpanel.people.set(user.id, mp_properties, function(err) {
      if (err) emergencyLog('Mixpanel userUpdate: ' + err, { exception: err });
    });
  });

}

function configureGA(statsHandlers, config) {
  var ua = require('universal-analytics');
  var gaID = config.get('stats:ga:key');
  if (!gaID) return emergencyLog(new Error('Missing key for Google Analytics'));

  statsHandlers.event.push(function(eventName, properties) {
    // Don't handle events that don't have a googleAnalyticsUniqueId
    if(!properties || !properties.googleAnalyticsUniqueId) return;
    var visitor = ua(gaID, properties.googleAnalyticsUniqueId);

    visitor.event({
      ec: "stats",
      ea: eventName,
      // Label: el: "",
      // Value: ev: 0,
      // Page: dp: "/"
    }, function(err) {
      if (err) emergencyLog('Google Analytics event error: ' + err, { exception: err });
    });

  });

  statsHandlers.trackRequest.push(function (req) {
    var UUID;

    // checking for existent UUID
    if (req.cookies && req.cookies._ga) {
      UUID = (req.cookies._ga).split('.').slice(2, 4).join('.');
    }

    var visitor = ua(gaID, UUID);
    visitor.pageview({
        dp: req.url,
        dh: req.get('host'),
        dr: req.get('referer'),
        ua: req.get('User-Agent')
      }, function (err) {
        if (err) emergencyLog('Google Analytics Track Request: ' + err, { exception: err });
      });
  });
}

exports.create = function(options) {
  // Stats requires a logger and logger requires stats, so
  // we'll forego logging in the stats module rather than the
  // other way around
  var config = options.config;
  var logger = options.logger;

  var statsHandlers = {
    trackRequest: [],
    event: [],
    charge: [],
    eventHF: [],
    gaugeHF: [],
    userUpdate: [],
    responseTime: [],
    alias: []
  };

  var mixpanelEnabled = config.get("stats:mixpanel:enabled");
  var gaEnabled = config.get('stats:ga:enabled');
  var statsdEnabled = config.get("stats:statsd:enabled");
  var cubeEnabled = config.get("stats:cube:enabled");
  var loggerEnabled = config.get("stats:logger:enabled");
  var intercomEnabled = config.get("stats:intercom:enabled");

  /**
   * console
   */
  if (loggerEnabled) {
    configureLogger(statsHandlers, config, logger);
  }

  /**
   * cube
   */
  if (cubeEnabled) {
    configureCube(statsHandlers, config);
  }

  /**
   * statsd
   */
  if (statsdEnabled) {
    configureStatsD(statsHandlers, config)
  }

  /**
   * Intercom
   */
  if (intercomEnabled) {
    configureIntercom(statsHandlers, config);
  }

  /**
   * Mixpanel
   */
  if (mixpanelEnabled) {
    configureMixpanel(statsHandlers, config);
  }

  /**
   * Request Tracking for Google Analytics
   */
  if (gaEnabled) {
    configureGA(statsHandlers, config);
  }

  function makeHandler(handlers) {
    if(!handlers.length) return function() {};

    if(handlers.length === 1) {
      /**
       * Might seem a bit over the top, but these handlers get
       * called a lot, so we should make this code perform if we can
       */
      var handler = handlers[0];
      return function() {
        var args = arguments;

        try {
          handler.apply(null, args);
        } catch(err) {
          emergencyLog('[stats] Error processing event: ' + err, { exception: err });
        }
      };
    }

    return function() {
      var args = arguments;

      handlers.forEach(function(handler) {
        try {
          handler.apply(null, args);
        } catch(err) {
          emergencyLog('[stats] Error processing event: ' + err, { exception: err });
        }
      });
    };
  }

  return Object.keys(statsHandlers).reduce(function(memo, method) {
    memo[method] = makeHandler(statsHandlers[method]);
    return memo;
  }, {});
};
