"use strict";

var responseTime = require('response-time');
var debug = require('debug')('gitter:http');

function getPathTag(req) {
  // This is put here by the identify-route middleware
  if (req.routeIdentifier) {
    return req.routeIdentifier;
  }

  // Fallback to the old method...
  var path = req.route && req.route.path;
  if (!path) {
    return;
  }

  path = path
          .replace(/(\.:format\?|\([^\)]*\))/g, '')
          .replace(/:/g, '_')
          .replace(/[^\w\-\/]/g, '');

  return path;
}

exports.create = function(options) {
  var config = options.config;

  var statsdClient = require('../stats-client').create({
      config: config,
      includeNodeVersionTags: true
    });

  return responseTime(function (req, res, time) {
    var routeTag = getPathTag(req);

    var tags = ['method:' + req.method, 'sc:' + res.statusCode];
    if (routeTag) {
      tags.push('route:' + routeTag);
    }

    debug("%s %s [%s] completed %s with in %sms", req.method, req.originalUrl, req.routeIdentifier, res.statusCode, time);

    statsdClient.histogram('http.request', time, tags);
  });

};
