'use strict';

var handlebarsCompiler = require('./handlebars-compiler');

function HandlebarsMemoizer() {
  this.compiled = {};
}

HandlebarsMemoizer.prototype = {
  get: function(sourceFileName) {
    if (this.compiled[sourceFileName]) {
      return this.compiled[sourceFileName];
    }

    var promise = handlebarsCompiler(sourceFileName);
    this.compiled[sourceFileName] = promise;
    return promise;
  }
};

module.exports = HandlebarsMemoizer;
