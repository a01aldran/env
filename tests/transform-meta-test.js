/*jshint trailing:false, unused:true, node:true */
/* global it:false, describe:false*/
"use strict";

var assert = require('assert');
var transformMeta = require('../lib/transform-meta');

function CustomObject() {
}

CustomObject.prototype.toString = function() {
  return "custom";
};

describe('transform-meta', function() {

  it('should transform an empty hash', function() {
    assert.deepEqual(transformMeta({}), {});
  });

  it('should transform an simple hash', function() {
    assert.deepEqual(transformMeta({ hello: 'there' }), { hello: 'there' });
    assert.deepEqual(transformMeta({ hello: 1 }), { hello: 1 });
    assert.deepEqual(transformMeta({ hello: false }), { hello: false });
    assert.deepEqual(transformMeta({ hello: null }), { hello: null });
    assert.deepEqual(transformMeta({ hello: undefined }), { });
  });

  it('should transform an two-level deep hash', function() {
    assert.deepEqual(transformMeta({ hello: { there: 1 } }), { hello: { there: 1 } });
  });

  it('should ignore anything deeper than two levels', function() {
    assert.deepEqual(transformMeta({ hello: { there: { world: 1 } } }), { hello: { } });
    assert.deepEqual(transformMeta({
      hello: {
        there: {
          world: 1
        }
      },
      world: {
        a: 1,
        b: { },
        c: new CustomObject()
      }
    }), {
      hello: { },
      world: {
        a: 1,
        c: "custom"
      }
    });
  });

});
