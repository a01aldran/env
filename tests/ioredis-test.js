"use strict";

var ioredis = require('../lib/env-ioredis');
var mockConfig = require('./mock-config');
var redisSpec = require('./redis-spec');

describe('gitter-redis', function() {
  var logger, factory;

  before(function() {
    logger = require('../lib/logger').create({ config: mockConfig({}) });
    factory = ioredis.create({config: mockConfig({}), logger: logger });
  });

  redisSpec(function(options) {
    return factory.createClient(options, logger);
  }, function(client) {
    factory.quitClient(client);
  });


});
