"use strict";

var Promise = require('bluebird');
var assert = require('assert');
var sinon = require('sinon');
var mockConfig = require('./mock-config');
var logger = require('../lib/logger');
var errorReporter = require('../lib/error-reporter')

describe('error-reporter', function() {
  var config;
  var loggerInstance;
  var errorReporterInstance;

  beforeEach(function() {
    config = mockConfig({
      "errorReporting": {
        "enabled": true
      }
    });

    // TODO We could do with mocks of these
    loggerInstance = logger.create({ config: config });
    errorReporterInstance = errorReporter.create({ config: config, logger: loggerInstance });
  });

  it('should log errors', function() {
    sinon.spy(loggerInstance, 'error');

    errorReporterInstance(new Error('some test error'), {}, {});

    assert.equal(loggerInstance.error.callCount, 0, 'should not encounter any errors when trying to report an error to Sentry');
  });
});
