"use strict";

var assert = require('assert');
var redisUrlParser = require('../lib/utils/redis-url-parser');

describe('redis-url-parser', function() {

  it('should parse a simple url', function() {
    assert.deepEqual(redisUrlParser('redis://localhost'), { host: 'localhost' });
  });

  it('should parse a simple url with port', function() {
    assert.deepEqual(redisUrlParser('redis://localhost:6379'), { host: 'localhost', port: 6379 });
  });

  it('should parse a simple url with port and db', function() {
    assert.deepEqual(redisUrlParser('redis://localhost:6379?db=1'), { host: 'localhost', port: 6379, redisDb: 1 });
  });

  it('should parse a simple url with port, db and clientOpts ', function() {
    assert.deepEqual(redisUrlParser('redis://localhost:6379?db=1&return_buffers=true'), { host: 'localhost', port: 6379, redisDb: 1, clientOpts: { return_buffers: true } });
  });

  it('should parse a sentinel connection', function() {
    assert.deepEqual(redisUrlParser('redis-sentinel://localhost:26379?master=mymaster&db=1'), {
      redisDb: 1,
      sentinel: {
        "master-name": "mymaster",
        "hosts": ["localhost:26379"]
      }
    });
  });

  it('should parse a sentinel connection with multiple hosts', function() {
    assert.deepEqual(redisUrlParser('redis-sentinel://host01:26379,host02:26379?master=mymaster&db=1'), {
      redisDb: 1,
      sentinel: {
        "master-name": "mymaster",
        "hosts": ["host01:26379", "host02:26379"]
      }
    });
  });

  it('should parse a sentinel connection with multiple hosts and client options', function() {
    assert.deepEqual(redisUrlParser('redis-sentinel://host01:26379,host02:26379?master=mymaster&db=1&return_buffers=true'), {
      redisDb: 1,
      sentinel: {
        "master-name": "mymaster",
        "hosts": ["host01:26379", "host02:26379"]
      },
      clientOpts: {
        return_buffers: true
      }
    });
  });

});
